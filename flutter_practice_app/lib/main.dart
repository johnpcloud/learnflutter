import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  home: Home(),
));

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Navbar'),
        centerTitle: true,
        backgroundColor: Colors.red[400],
      ),
      body: Center(

          //Image
//        child: Image(
//          //image: NetworkImage('https://www.photographymad.com/files/images/star-trails-reflected-in-lake.jpg'),
//          image: AssetImage('assets/s3.jpg'),
//        ),
//        child: Image.asset('assets/s1.jpg'),
//        child: Image.network('https://www.photographymad.com/files/images/star-trails-reflected-in-lake.jpg'),

            //Icon
          child: Icon(
            Icons.airport_shuttle,
            color: Colors.red,
            size: 80,
          ),

              //RaisedButton
//          child: RaisedButton(
//            onPressed: () {},
//            child: Text('Tap me'),
//            color: Colors.lightBlue,
//          ),

//          child:FlatButton(),


      ),
      floatingActionButton: FloatingActionButton(
          onPressed: (){},
          child: Text('Click'), //const Icon(Icons.add),
      ),
    );
  }
}
