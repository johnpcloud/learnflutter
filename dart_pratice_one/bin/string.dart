// String, Literals and String interpolation
void main() {
  
  // Literals
  true; 2; "Peddlecloud"; 4.5;

  // Various way to define String Literals
  String s1 = 'Single';
  String s2 = "Double";
  String s3 = 'It\'s easy';
  String s4 = "It's easy";
  String s5 = 'This is going to be a very long message. '
              'This message is just a sample message for Dart programming string testing';
  
  // String interpolation : use "My name is $name" instead of "My name is " + name
  String name = "PavDev";
  String message = "The router name is $name and the name length is ${name.length}";

  print(message);
}