void main() {
  
  // Defining Lambda: 1st way
  Function addTwoNumber = (int a, int b) { print(a+b); };

  var pentaValue = (int number) { return number * 5; };



  // Defining Lambda: 2nd way: Function Expression: Using Short Hand Syntax or FAT Arrow ( '=>' )
  Function addNumber = (int a, int b) => print(a+b);

  var pentaNumber = (int number) => number * 5;

  //  Show Result
  addTwoNumber(4,6);
  print(pentaValue(6));
  print("");
  addNumber(10,15);
  print(pentaNumber(8));
}