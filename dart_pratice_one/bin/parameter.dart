void main () {
  cities("Dhaka", "Chittagong", "Rajshahi");
  print("");
  // Without optional parameter
  //country("Bangladesh", "India", "USA");
  // With optional parameter
  country("Bangladesh");
  print("");
  continent("Asia", c2:"Europe", c3:"Africa");
  print("");
  //  Default parameter
  print(findVolumn(2, 3, h:5));
}

//  Required Parameters
void cities(String c1, String c2, String c3){
  print("Name 1 is $c1");
  print("Name 2 is $c2");
  print("Name 3 is $c3");
}

//  Optional Parameters
void country(String c1, [String c2, String c3]){
  print("Name 1 is $c1");
  print("Name 2 is $c2");
  print("Name 3 is $c3");
}

//  Named Parameters
void continent(String c1, {String c2, String c3}){
  print("Name 1 is $c1");
  print("Name 2 is $c2");
  print("Name 3 is $c3");
}

//  Optional Named Parameters
int findVolumn(int l, int b, {int h = 10}){
  return l*b*h;
}