// Datatype
void main() {

  //Number: int
  //int score = 50;
  var score = 70;
  
  //Number: double
  //double percentage = 89.54;
  var percentage = 92.36;

  //String
  //String name = 'Peddlecloud';
  String name = 'Pavillion';

  //Boolean
  //bool isActive = true;
  bool isActive = false;


  //Print the variables
  print('The name is $name');
  print('Is the company valid? Ans is $isActive');
  print('Achived score is $score');
  print('Gained percentage is $percentage %');
}