import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  home: Home(),
));

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Navbar'),
        centerTitle: true,
        backgroundColor: Colors.red[400],
      ),
//      body: Center(

        //Image
//        child: Image(
//          //image: NetworkImage('https://www.photographymad.com/files/images/star-trails-reflected-in-lake.jpg'),
//          image: AssetImage('assets/s3.jpg'),
//        ),
//        child: Image.asset('assets/s1.jpg'),
//        child: Image.network('https://www.photographymad.com/files/images/star-trails-reflected-in-lake.jpg'),

        //Icon
//        child: Icon(
//          Icons.airport_shuttle,
//          color: Colors.blue,
//          size: 80,
//        ),

        //RaisedButton
//          child: RaisedButton(
//            onPressed: () {},
//            child: Text('Tap me'),
//            color: Colors.lightBlue,
//          ),

//          child:FlatButton(
//            onPressed: () {
//              print('Flat Button clicked');
//            },
//            child: Text('CLICKING'),
//            color: Colors.red,
//          ),

//          child:RaisedButton.icon(
//            onPressed: () {},
//            icon: Icon(
//              Icons.mail
//            ),
//            label: Text("Mail Me"),
//            color: Colors.amber,
//          )

//              child:IconButton(
//                onPressed: () {
//                  print('Icon Button clicked');
//                },
//                icon: Icon(Icons.alternate_email),
//                color: Colors.amber,
//              )
//      ),

//      body: Container(
//        //padding: EdgeInsets.all(20.0),
//        padding: EdgeInsets.fromLTRB(10.0,20.0,10.0,30.0),
//
//        margin: EdgeInsets.fromLTRB(10.0,20.0,10.0,30.0),
//
//        color: Colors.grey[400],
//        child:Text(
//          'hello',
//          style: TextStyle(
//            color: Colors.white,
//            fontSize: 20,
//          ),
//        ),
//      ),

//      body : Row(
//        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//        crossAxisAlignment: CrossAxisAlignment.start,
//        children: <Widget>[
//          Text('Welcome'),
//          FlatButton(
//            onPressed: (){},
//            color: Colors.amber,
//            child: Text('Tap me'),
//          ),
//          Container(
//            color: Colors.cyan,
//            padding: EdgeInsets.all(18.0),
//            child: Text('Inside the container'),
//          ),
//        ],
//      ),


//      body:Column(
//        children: <Widget>[
//          Container(
//            padding: EdgeInsets.all(20.0),
//            color: Colors.red,
//            child: Text('One'),
//          ),
//          Container(
//            padding: EdgeInsets.all(40.0),
//            color: Colors.yellow,
//            child: Text('Two'),
//          ),
//          Container(
//            padding: EdgeInsets.all(60.0),
//            color: Colors.blue,
//            child: Text('Three'),
//          ),
//        ],
//      ),

      body: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.all(30.0),
              color: Colors.cyan,
              child: Text('1'),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(30.0),
              color: Colors.pink,
              child: Text('2'),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(30.0),
              color: Colors.amber,
              child: Text('3'),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){},
        child: Text('Click'), //const Icon(Icons.add),
      ),
    );
  }
}