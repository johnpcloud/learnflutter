import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SignupPage(),
    );
  }
}

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final formKey = GlobalKey<FormState>();
  //String _name, _email, _password;

  Future<void> registration() async {
//    if(formKey.currentState.validate()){
//      formKey.currentState.save();
//
//    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
      child: Form(
        key: formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(15.0, 90.0, 0.0, 0.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          '_',
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 40.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.green
                              ),
                        ),
                        SizedBox(width: 0.2),
                        Text(
                          'teamPavilion',
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 40.0,
                              fontWeight: FontWeight.bold,
                              ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.fromLTRB(15.0, 120.0, 0.0, 0.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          'join us',
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 80.0,
                              fontWeight: FontWeight.bold,
                              letterSpacing: 4.1),
                        ),
                        SizedBox(width: 1.0),
                        Text(
                          '.',
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 90.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.green),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(top: 25.0, left: 30.0, right: 30.0),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    //onSaved: (input) => _name = input,
                    decoration: InputDecoration(
                      labelText: 'NAME',
                      labelStyle: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          color: Colors.grey),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),
                    ),
                    validator: (input) => input.isEmpty && input.length < 3 ? 'Enter your name' : null,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    //onSaved: (input) => _email = input,
                    decoration: InputDecoration(
                      labelText: 'EMAIL',
                      labelStyle: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          color: Colors.grey),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),
                    ),
                    validator: (input) => !input.contains('@') ? 'Not a valid Email' : null,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    //onSaved: (input) => _password = input,
                    decoration: InputDecoration(
                      labelText: 'PASSWORD',
                      labelStyle: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          color: Colors.grey),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),
                    ),
                    obscureText: true,
                    validator: (input) => input.length < 7 ? 'Password must be more then 7 characters' : null,
                  ),
                  SizedBox(
                    height: 35.0,
                  ),
                  Container(
                    height: 40.0,
                    child: Material(
                      borderRadius: BorderRadius.circular(20.0),
                      shadowColor: Colors.greenAccent,
                      color: Colors.green,
                      elevation: 7.0,
                      child: GestureDetector(
                        onTap: registration,
//                        onTap: () {
//                          Navigator.of(context).pushNamed('/homePage');
//                        },
                        child: Center(
                          child: Text(
                            'SIGNUP',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Poppins'),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
//                Container(
//                  height: 40.0,
//                  child: Material(
//                    borderRadius: BorderRadius.circular(20.0),
//                    elevation: 7.0,
//                    child: GestureDetector(
//                      onTap: () {
//                        Navigator.of(context).pushNamed('/logIn');
//                      },
//                      child: Center(
//                        child: Text(
//                          '<< GO BACK',
//                          style: TextStyle(
//                              color: Colors.black,
//                              fontWeight: FontWeight.bold,
//                              fontFamily: 'Poppins'),
//                        ),
//                      ),
//                    ),
//                  ),
//                ),
//                SizedBox(
//                  height: 15.0,
//                )
                ],
              ),
            ),
          ],
        ),
      ),
      )
    );
  }
}
