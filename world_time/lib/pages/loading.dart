import 'package:flutter/material.dart';
import 'package:world_time/services/world_time.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {

  String time = 'Loading';

  void setWorldTime() async {

    WorldTime ins = WorldTime(location: 'Dhaka', flag: 'bangladesh.png', url: 'Asia/Dhaka');
    await ins.getTime();
//    print(ins.time);
//    setState(() {
//      time = ins.time;
//    });
    Navigator.pushReplacementNamed(context, '/home', arguments: {
      'location': ins.location,
      'flag': ins.flag,
      'time': ins.time,
      'isDayTime': ins.isDayTime,
    });
  }

  @override
  void initState() {
    super.initState();
    setWorldTime();
    //getTime();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SpinKitRing(
          color: Colors.blueAccent,
          size: 80.0,
        ),
      ),
    );
  }
}
