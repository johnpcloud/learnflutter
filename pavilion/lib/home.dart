import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  void initState() {
    getUserName();
    super.initState();
  }


  String userName="";

  /*Future<String>*/ getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getString('user_id') != null) {
      print('From getUserName, user_name =  ${prefs.getString('user_name')}');

//    return prefs.getString('user_name') ?? '';

      setState(() {
        userName = prefs.getString('user_name') ?? '';
      });
    }else{
      Navigator.of(context).pushNamed('/homePage');
    }
  }


  //String userName = getUserName();




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
//          'Profile Page',
          'Name : $userName',
          style: TextStyle(
            color: Colors.black,
            fontFamily: 'Poppins',
            fontSize: 50.0,
            fontWeight: FontWeight.bold,
            letterSpacing: 6.1,
          ),
        ),
      ),
    );
  }
}
